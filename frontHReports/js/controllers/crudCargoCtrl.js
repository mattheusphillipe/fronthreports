var app = angular.module("hreports").controller("crudCargoCtrl", function ($scope, $http,$location) {

    $scope.jobs = [];
    $scope.departamentos = [];

    $scope.paginaAtual = 1;

    var countPages = function () {
        $http.get("http://localhost:8084/job/countJobs").then(function (response) {
            $scope.paginaMax = Math.ceil(response.data / $scope.pageSize);
            console.log($scope.paginaMax);
        });
    }
    countPages();


    var carregarjobs = function () 
    {
        $http.get("http://localhost:8084/job/findAll").then(function (response) {
            $scope.jobs = response.data;
            console.log($scope.jobs);
        });

    };

    var carregarDepartments = function (){
        $http.get("http://localhost:8084/department/findAll").then(function(response){
            $scope.departamentos = response.data;
            console.log($scope.departamentos);
        });
    };

    carregarDepartments();

    $scope.vincularCargoADepart = function (id, job){
            $http.post("http://localhost:8084/department/addJob" + "?id=" + id, job).then(function(response){
                carregarjobs();
            });
    };

    var parse = function (){
        $scope.Jobadd.department = JSON.parse($scope.Jobadd.department);
    };


    $scope.adicionarcargo = function (Job) {
        parse();
        $http.post("http://localhost:8084/job/save ", Job).then(function (response) {
            $scope.jobs.push(angular.copy(Job));
            carregarjobs();

        

            if (Math.ceil($scope.jobs.length / $scope.pageSize) > $scope.paginaMax) {
                $scope.paginaMax++;
            }

        });

        //  delete $scope.Jobadd;


    };

    $scope.copiarDadosModal = function (Job) {
        $scope.jobSelecionado = angular.copy(Job);
    }

    $scope.jobstatus = ["Ativo", "Inativo"];

    $scope.deleteJob = function (Job) {
        urlDelete = "http://localhost:8084/job/deleteById" + "?id=" + Job.idJob;
        console.log("Aqui:" + urlDelete);
        $http.delete(urlDelete).then(function (response) {
            carregarjobs();
            if (Math.ceil($scope.jobs.length / 5) < $scope.paginaMax) {
                $scope.paginaMax--;
            }

        })
    }

    $scope.updateJob = function (Job) {
        console.log(Job.jobName);
        $http.put("http://localhost:8084/job/update", Job).then(function (response) {
            carregarjobs();
        })
    }

    carregarjobs();


    $scope.paginaProxima = function () {
        if ($scope.currentPage + 1 < $scope.paginaMax) {

            $scope.currentPage++;
        }

    }

    $scope.paginaAnterior = function () {
        if ($scope.currentPage - 1 > -1) {
            $scope.currentPage--;
        }

    }

    $scope.currentPage = 0;
    $scope.pageSize = 8; // it was 5

    $scope.mudarPagina = function (num_pag) {
        $scope.currentPage = num_pag;
    }

});

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})