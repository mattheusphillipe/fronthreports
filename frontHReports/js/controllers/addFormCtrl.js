var app = angular.module("hreports").controller("addFormCtrl", function ($scope, formularioAPI) {
    $scope.form = null;
    $scope.alternativesToQuestion = [];
    $scope.questionsToForm = [];
    $scope.questionsToShow = [];

    $scope.showAlternatives = function (questionSelected) {
        $scope.questionSelected = questionSelected;
    }

    $scope.newAlternative = function (alternative,question) {
            //Variavel para inserir alternativas no banco
            alternative.question = question;
            $scope.alternativesToQuestion.push(angular.copy(alternative));

            //Limpar o campo
            delete $scope.alternative;
        }
        //Funcao para adicionar as alternativas e inseri-las na questao
        $scope.addQuestion = function (questionToSave, alternativesToQuestion) {

            formularioAPI.saveQuestion(questionToSave).then(function (
                response) {
                var idSavedQuestion = response.data.idQuestion;
                console.log("idSavedQuestion aqui");
                console.log(idSavedQuestion);

                formularioAPI.saveAlternative(alternativesToQuestion).then(function (
                    response) {
                    var savedAlternatives = response.data;

                    formularioAPI.getQuestion(idSavedQuestion).then(function (response) {

                        //Variavel para inserir questoes no banco
                        $scope.questionsToForm.push(angular.copy(response.data));
                     

                        //Variavel para armazenar a questão salva
                        var data = response.data;
                        //atribuir as alternativas salvas salvas
                        console.log("verificação alternativas salvas da função")
                        data.alternatives = savedAlternatives;

                        //Variavel para mostrar as questoes
                        $scope.questionsToShow.push(angular.copy(data));

                        formularioAPI.saveAlternativeQuestion(
                            idSavedQuestion,
                            savedAlternatives).then(function (response) {});
                    });

                    });

                delete $scope.alternativesToQuestion;
                $scope.alternativesToQuestion = [];
                delete $scope.question;
            });
        };
        // Funcao para salvar o formulario e todas as questões no formulario 
        $scope.addData = function (obj) {
            console.log("OBJ: " + obj.formName);

            formularioAPI.saveForm(obj).then(function (response) {
                var idForm = response.data.idForm;  
                // data.alternatives = savedAlternatives;
                //comentei esse data.alternatives e estou passando $scope.questionsForm e esta dando certo
               

                formularioAPI.saveQuestionForm(idForm, $scope.questionsToForm).then(function (response) {});
                    alert("formulario cadastrado com sucesso"+ idForm );
            });
            delete $scope.alternativesToQuestion;
            $scope.alternativesToQuestion = [];
        };

    });
