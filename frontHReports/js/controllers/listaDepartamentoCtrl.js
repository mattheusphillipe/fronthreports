angular.module("hreports").controller("listaDepartamentoCtrl", function($scope, departamentos, contDepartamentos, departamentoAPI, $location){
    $scope.departamentos = departamentos.data;
    $scope.contDepartamentos = contDepartamentos.data;
    $scope.paginaAtual = 1;
    $scope.currentPage = 0;
    $scope.pageSize = 8;// it was 4
    $scope.paginaMax  = Math.ceil(contDepartamentos.data / $scope.pageSize);
    console.log($scope.paginaMax);
    console.log($scope.contDepartamentos);

    $scope.title = "Novo departamento";
    $scope.adicionarDepartamento = function(departamento){
        departamentoAPI.saveDepartamento(departamento).then(function(data){

            if (Math.ceil($scope.departamentos.length / $scope.pageSize) > $scope.paginaMax) {
                $scope.paginaMax++;
            }

            delete $scope.departamento;
            $scope.departamentoForm.$setPristine();
            $location.path("/dep");
            
        }); 
       
    };

    $scope.copiarDadosModal = function(departamento){
        $scope.deparSelecionado = angular.copy(departamento);

    };

    $scope.alterarDepartamento = function(deparSelecionado){
        departamentoAPI.updateDepartamento(deparSelecionado).then(function(data){
            delete $scope.deparSelecionado;
            $scope.departamentoForm.$setPristine();
            $location.path("/dep");
            

        });
    };

    $scope.deletarDepartamento = function (deparSelecionado){
        departamentoAPI.deleteDepartamento(deparSelecionado).then(function(data){

            if (Math.ceil($scope.departamentos.length / 4) < $scope.paginaMax) {
                $scope.paginaMax--;
            }

            delete $scope.deparSelecionado;
            $location.path("/dep");
        });
    };

    $scope.paginaProxima = function () {
        if ($scope.currentPage + 1 < $scope.paginaMax) {

            $scope.currentPage++;
        }

    }

    $scope.paginaAnterior = function () {
        if ($scope.currentPage - 1 > -1) {
            $scope.currentPage--;
        }

    }


    $scope.mudarPagina = function (num_pag) {
        $scope.currentPage = num_pag;
    }

});