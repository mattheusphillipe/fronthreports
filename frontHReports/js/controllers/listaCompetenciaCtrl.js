angular.module("hreports").controller("listaCompetenciaCtrl", function($scope, competencias, contCompetencias, competenciaAPI, $location){
    $scope.competencias = competencias.data;
    $scope.contCompetencias = contCompetencias.data;
    $scope.paginaAtual = 1;
    $scope.currentPage = 0;
    $scope.pageSize = 4;
    $scope.paginaMax = Math.ceil(contCompetencias.data / $scope.pageSize);
    console.log($scope.paginaMax);
    console.log($scope.contCompetencias);

    $scope.title = "Nova competencia";
    $scope.adicionarCompetencia = function(competencia){
        competenciaAPI.saveCompetencia(competencia).then(function(data){

           
            if (Math.ceil($scope.competencias.length / $scope.pageSize) > $scope.paginaMax) {
                $scope.paginaMax++;
            }
            delete $scope.competencia;
            $scope.competenciaForm.$setPristine();         
            $location.path("/comp");

            
        });
       
    };

    $scope.copiarDadosModal = function(competencia){
        $scope.compSelecionado = angular.copy(competencia);

    };

    $scope.alterarCompetencia = function(compSelecionado){
        competenciaAPI.updateCompetencia(compSelecionado).then(function(data){
            delete $scope.compSelecionado;
            $scope.competenciaForm.$setPristine();
            $location.path("/comp");
            

        });
    };

    $scope.deletarCompetencia = function (compSelecionado){
        competenciaAPI.deleteCompetencia(compSelecionado).then(function(data){

            if (Math.ceil($scope.competencias.length / 4) < $scope.paginaMax) {
                $scope.paginaMax--;
            }

            delete $scope.compSelecionado;
            $location.path("/comp");
        });
    };


    $scope.paginaProxima = function () {
        if ($scope.currentPage + 1 < $scope.paginaMax) {

            $scope.currentPage++;
        }

    }

    $scope.paginaAnterior = function () {
        if ($scope.currentPage - 1 > -1) {
            $scope.currentPage--;
        }

    }


    $scope.mudarPagina = function (num_pag) {
        $scope.currentPage = num_pag;
    }
    

});