angular.module("hreports").factory("departamentoAPI", function($http, config){
    var _getDepartamentos = function(){
        return $http.get(config.baseUrl + "/department/findAll");
    };

    var _contDepartamentos = function(){
        return $http.get(config.baseUrl + "/department/countDepartments");
    };

    var _getDepartamento = function(id){
        return $http.get(config.baseUrl + "/department/findById/" + id);
    };

    var _saveDepartamento = function (departamento){
        return $http.post(config.baseUrl + "/department/save", departamento);
    };

    var _updateDepartamento = function (departamento){
        return $http.put(config.baseUrl + "/department/update", departamento);
    };

    var _deleteDepartamento = function (departamento){
        return $http.delete(config.baseUrl + "/department/deleteById" + "?id=" + departamento.idDepartment);
    };

    return {
        getDepartamentos: _getDepartamentos,
        getDepartamento: _getDepartamento,
        saveDepartamento: _saveDepartamento,
        updateDepartamento: _updateDepartamento,
        deleteDepartamento: _deleteDepartamento,
        contDepartamentos: _contDepartamentos

    };

});