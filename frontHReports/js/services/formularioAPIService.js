angular.module("hreports").factory("formularioAPI", function($http, config){

	var _getQuestion = function(idSavedQuestion){
		return $http.get(config.baseUrl + "/question/findById?id="+ idSavedQuestion);
	};

	var _saveQuestion = function(questionToSave){
		return $http.post(config.baseUrl + "/question/save",questionToSave);
	};

	var _saveAlternative = function (alternativesToQuestion){
		return $http.post(config.baseUrl + "/alternative/saveAll", alternativesToQuestion);
	};

	var _saveAlternativeQuestion = function (idSavedQuestion,savedAlternatives){
		return $http.post(config.baseUrl + "/question/addAlternatives?id="+ idSavedQuestion,savedAlternatives);
	};

	var _saveForm = function (obj){
		return $http.post(config.baseUrl + "/form/save" ,obj);
	};

	var _saveQuestionForm = function (idForm, questionsToForm){
		return $http.post(config.baseUrl + "/form/addQuestions?id= " + idForm, questionsToForm);
	};

	return {

		getQuestion:_getQuestion,
		saveQuestion:_saveQuestion,
		saveAlternative: _saveAlternative,
		saveAlternativeQuestion:_saveAlternativeQuestion,
		saveForm:_saveForm,
		saveQuestionForm:_saveQuestionForm
	};

});