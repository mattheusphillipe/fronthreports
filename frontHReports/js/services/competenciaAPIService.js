angular.module("hreports").factory("competenciaAPI", function($http, config){
    var _getCompetencias = function(){
        return $http.get(config.baseUrl + "/competence/findAll");
    };

    var _contCompetencias = function(){
        return $http.get(config.baseUrl + "/competence/countCompetences");

    };

    var _getCompetencia = function(id){
        return $http.get(config.baseUrl + "/competence/findById/" + id);
    };

    var _saveCompetencia = function (competencia){
        return $http.post(config.baseUrl + "/competence/save", competencia);
    };

    var _updateCompetencia = function (competencia){
        return $http.put(config.baseUrl + "/competence/update", competencia);
    };

    var _deleteCompetencia = function (competencia){
        return $http.delete(config.baseUrl + "/competence/deleteById" + "?id=" + competencia.idCompetence);
    };

    return {
        getCompetencias: _getCompetencias,
        getCompetencia: _getCompetencia,
        saveCompetencia: _saveCompetencia,
        updateCompetencia: _updateCompetencia,
        deleteCompetencia: _deleteCompetencia,
        contCompetencias: _contCompetencias 

    };

});