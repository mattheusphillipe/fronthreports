angular.module("hreports").config(function($routeProvider){
    $routeProvider.when("/departamentos", {
        templateUrl: "view/departamentos.html",
        controller: "listaDepartamentoCtrl",
        resolve: {
            departamentos: function(departamentoAPI){
                return departamentoAPI.getDepartamentos();
            },

            contDepartamentos: function(departamentoAPI){
                return departamentoAPI.contDepartamentos();
            }
        }
    });

    $routeProvider.when("/addForm", {
        templateUrl: "view/adicionarFormularioAlternative.html",
        controller: "addFormCtrl"
    });

    $routeProvider.when("/relatorio", {
        templateUrl: "view/relatorio.html",
        controller: "relatorioCtrl"
    });

    $routeProvider.when("/dashboard", {
        templateUrl: "view/dashboard.html",
        controller: "dashboardCtrl"
    });

    $routeProvider.when("/controlpainel", {
        templateUrl: "view/painel.html",
    });

    $routeProvider.when("/listarForm", {
        templateUrl: "view/listaFormulario.html",
        controller: "listaFormsCtrl"

    });

    $routeProvider.when("/jobs", {
        templateUrl: "view/job.html",
        controller: "crudCargoCtrl"

    });


    $routeProvider.when("/pesquisa", {
        templateUrl: "view/pesquisa.html",
        controller: "listaPesquisaCtrl"

    });


    $routeProvider.when("/competencias", {
        templateUrl: "view/competencias.html",
        controller: "listaCompetenciaCtrl",
        resolve: {
            competencias: function(competenciaAPI){
                return competenciaAPI.getCompetencias();
            },

            contCompetencias: function(competenciaAPI){
                return competenciaAPI.contCompetencias();
            }
        }
    });

   
    $routeProvider.when("/error", {
        templateUrl: "view/error,html"
    });


    $routeProvider.when("/comp",{redirectTo: "/competencias"});
    $routeProvider.when("/dep", {redirectTo: "/departamentos"});   
    $routeProvider.otherwise({redirectTo: "/controlpainel"});

 
    
});